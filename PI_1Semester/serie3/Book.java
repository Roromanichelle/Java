/* ************************************************************************* *\
*                Programmierung 1 HS 2019 - Serie 3-1                         * 
\* ************************************************************************* */

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 11.10.2019
 */

import java.util.Date;
import java.util.Scanner;
import java.text.*;

public class Book 
{

	private int id;
	private String title;
	private String author;
	private Date dateOfPublication;
	
	public static final String DATE_FORMAT = "dd.MM.yyyy";

	//--- constructors ---	

	public Book(int newid, String newtitle, String newauthor, Date newdate) 
	{
		id = newid ;
		author = newauthor ;
		dateOfPublication = newdate ;
		title = newtitle ;
	}

	// TODO: Insert your code here!

	/** Returns the age of the book in days since publication */
	public int age(Date dateOfPublication)
	{	
		// TODO: Insert your code here!
		long millis_age = System.currentTimeMillis() - dateOfPublication.getTime() ; 
		return (int)millis_age/1000*60*60*24 ; // convert long into int
		
	}



	/** Reads all book data from user input */
	public void input() 
	{
		// TODO: Insert your code here!	
		Scanner sc = new Scanner(System.in);
		System.out.print( "Please enter id: " );
		id = sc.nextInt() ;
		System.out.println("Please enter the title : ") ;
		title = sc.nextLine() ;
		System.out.println("Please enter the author : ") ;
		author = sc.nextLine() ; 
		System.out.println("Please enter the date of Publication : ") ;
		dateOfPublication = stringToDate(sc.nextLine()) ;
	}

	//--- Get-/Set-methods ---

	// TODO: Insert your code here!
	
	public void setId(int i){
		id = i ;
	}

	public void setTitle(String t){
		title = t ;
	}		

	public void setAuthor(String a){
		author = a ;
	}
	
	public void setDateOfpublication(Date d){
		dateOfPublication = d ;
	}

	public int getId() {
		return id ;
	}

	public String getTitle() {
		return title ;
	}

	public String getAuthor() {
		return author ;
	}

	public Date getDateOfpublication() {
		return dateOfPublication ;

	}


	/** Returns a String representation of the book */
	public String toString()
	{
	// TODO: Insert your code here!
		return  id + ", " + author + ", " + dateToString(dateOfPublication) + ", " + title ;
	}


	//--- helper methods -- DO NOT CHANGE ------------------------------------
	/** Converts the Date object d into a String object */
	public static String dateToString( Date d )
	{
		SimpleDateFormat fmt = new SimpleDateFormat( DATE_FORMAT );
		return fmt.format( d );
	}

	/** Converts the String object s into a Date object */
	public static Date stringToDate( String s ) 
	{
		Date r = null;
		try {
			SimpleDateFormat fmt = new SimpleDateFormat( DATE_FORMAT );
			r = fmt.parse( s );
		} catch ( ParseException e ){
			System.err.println( e );
			System.exit(1);
		}
		return r;
	}
	
}

