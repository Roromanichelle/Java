import java.util.Date;
import java.util.Scanner;
import java.text.*;

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 11.10.2019
 */

public class Order {

    static int id = 0;

    private String costumerName;
    private String costumerAddress;
    private Book book1;
    private Book book2;
    private Book book3;
    private Book book4;
    private Book book5;
    private int i = 0 ;

    public Order()
    {
        id++ ; //Increment the number of the Order
    } 

    public void addBook(Book book)
    {
        if (i == 0) {
            book1 = book;
        }
        else if(i == 1) {
            book2 = book;
        }
        else if(i == 2) {
            book3 = book;
        }
        else if(i == 3) {
            book4 = book;
        }
        else if(i == 4) {
            book5 = book;
        }
        i++ ;
    }

    public void setCustomerName(String name)
    {
        costumerName = name ;
    }

    public void setCustomerAddress(String address)
    {
        costumerAddress = address ;
    }

    public String getCustomerName()
    {
        return costumerName ;
    }

    public String getCusomerAddress()
    {
        return costumerAddress ;
    }

    public void Input() 
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the Name of the costumer : ") ;
        costumerName = sc.nextLine() ;
        System.out.println("Please enter the Address of the costumer : ") ;
        costumerAddress = sc.nextLine() ;
    }

    public String toString()
	{
        String s = "Order id : " + id + ", " + "Costumer : " + costumerName + ", " + costumerAddress ;
        if (book1 != null) {
            s = s + "\n" + book1 ;
        }
        if (book2 != null) {
            s = s + "\n" + book2 ;
        }
        if(book3 != null) {
            s = s + "\n" + book3 ;
        }
        if(book4 != null) {
            s = s + "\n" + book4 ;
        }
        if(book5 != null) {
            s = s + "\n" + book5 ;
        }

        return s ;
	}
}

