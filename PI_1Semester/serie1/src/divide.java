/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */

import java.util.Scanner;

class divide {
    public static void main(String[] args) {

        // Get the first integer
        Scanner sc = new Scanner(System.in);                // Create new input scanner
        System.out.println("Please enter an integer :");
        int a = sc.nextInt();                               // Read an integer from de keyboard
        System.out.println("you entered : " + a);           // Say to the user what he entered
        
        // Get the second integer (shouldn't be 0)
        int b=0;                                            // Initiate variable b to 0         
        do {
            System.out.println("Please enter a second integer :");
            b = sc.nextInt();                               // Read an integer from de keyboard
            if(b==0) {                                      // Check if the integer is 0                             
                System.out.println("The number should not be 0");  // Warning message
            }
        } while (b==0);                                     // Redo while the user enter 0
        System.out.println("you entered : " + b);
        
        sc.close();                                         // close the input scanner

        // Calculate the result
        int quotient = (a*a) / b ;                          // Calculate the quotient
        int rest = (a*a) % b ;                              // Calculate de rest of the division
        
        System.out.println("The rest of the division is :" + rest + "\nThe quotient of the division is :" + quotient);
    }
}
