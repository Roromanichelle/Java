public class Fibonacci {
    public static long fib(int i) {
        int a=2;
        long b=1;
        if (i==0){
            b=0;
        }
        while (a<=i) {
            b=fib(a-1)+fib(a-2);
            a+=1;
        }        
        return b;
    }

    public static void main(String[] args){
        int i=0; 
        while (i<=50) {
            System.out.println(fib(i));
            i+=1;
        } 
        
    }
}