 import java.util.*;


 public class Factorial  {
    public static long factorial (int n){
        int a=1;
        long b=1;
        while(a<=n) {
            b=a*factorial(a-1);   
            a+=1;
        }
        return b ;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int f = sc.nextInt() ;
        System.out.println(factorial(f));
// To do : java Factorial 10 in the console
    }
 } 
    