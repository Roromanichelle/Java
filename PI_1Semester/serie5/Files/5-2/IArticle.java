/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */

 
public interface IArticle{
    public int getId() ; 
    public int getPrice() ;
    public String getDescription() ;

}