/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */



import java.util.ArrayList;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.ListIterator;


public class Order {

    private String customerName;
    private String customerAddress;
    private ArrayList<IArticle> orderedArticles ;
    public static int count;
    public int id;

    // Constructor 

    public Order() {
        orderedArticles = new ArrayList<IArticle>() ;
        count++; 
        id = count;
    }

    public void setCustomerName(String name) {
        customerName = name;
    }
    public void setCustomerAddress(String address) {
        customerAddress = address;
    }

    public int getId(){
        return id ;
    }

    public String getCustomerName(){
        return customerName;
    }

    public String getCustomerAddress(){
        return customerAddress;
    }

    public Iterable<IArticle> getOrderedArticles() {; 
        return orderedArticles ;
    }

    public int getTotalPrice() {
        int price = 0;
        for (IArticle a : orderedArticles){
            price += a.getPrice();
        }
        return price;
    }
    
    public void add(IArticle a) {
        this.orderedArticles.add(a);
    }
}
    
   