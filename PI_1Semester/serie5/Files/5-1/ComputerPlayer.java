/* ************************************************************************* *\
*                Programmierung 1 HS 2019 - Serie 5-1                         * 
\* ************************************************************************* */

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */


public class ComputerPlayer extends VierGewinnt implements IPlayer  {
	
	private Token token;

	/**
	 * Strategy is to chose a random column and select the next valid column to the
	 * right (the chosen included)
	 */
	public int getNextColumn(Token[][] board) {

		// TODO IMPROVE!

		int col = -1;
		java.util.Random generator = new java.util.Random();
		if (checkColumns(board)!=-1) {
			col = checkColumns(board) ;
		}
		else if (checkDiagonalsLeft(board)!=-1) {
			col = checkDiagonalsLeft(board) ;
		}
		else if (checkRows(board)!=-1) {
			col = checkRows(board) ;
		}
		else if (checkDiagonalsRight(board)!=-1) {
			col = checkDiagonalsRight(board);
		}
		else {
			col = generator.nextInt(board.length);
			while (isColFull(col, board)) {
				col = (col + 1) % board.length;
			}
		}
		return col ;

	}


	private int checkRows(Token[][] board) {

		int getcol = -1 ;
		boolean found = false ;
		int i = 0 ;
		while(i<4 && !found){
			int j = 0 ;
			while (j<ROWS){
				if(board[i][j]==Token.player2
				&& board[i+1][j]==Token.player2
				&& board[i+2][j]==Token.player2
				&& board[i+3][j]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				else if (board[i][j]==Token.player1
					&& board[i+1][j]==Token.player1
					&& board[i+2][j]==Token.player1
					&& board[i+3][j]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				j++;
			}
			i++;
		}	
		return getcol ;
	}

	private int checkColumns(Token[][] board) {
		int getcol = -1 ;
		boolean found = false ;
		int i = 0 ;
		while(i<COLS && !found){
			int j = 0 ;
			while (j<3 && !found){
				if(board[i][j]==Token.player2
					&& board[i][j+1]==Token.player2
					&& board[i][j+2]==Token.player2
					&& board[i][j+3]==Token.empty){
						found = true ;
						getcol = i ;
						break;
					}
				else if(board[i][j]==Token.player1
					&& board[i][j+1]==Token.player1
					&& board[i][j+2]==Token.player1
					&& board[i][j+3]==Token.empty){
						found = true;
						getcol = i;
						break;
					}
				j++;
			}
			i++;
		}	
		return getcol ;
	}

	private int checkDiagonalsRight(Token[][] board) {
		int getcol = -1 ;
		boolean found = false ;
		int i = 0 ;
		while(i<4 && !found){
			int j = 0 ;
			while (j<3 && !found){
				if(board[i][j]==Token.player2
					&& board[i+1][j+1]==Token.player2
					&& board[i+2][j+2]==Token.player2
					&& board[i+3][j+2]!=Token.empty
					&& board[i+3][j+3]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				else if (board[i][j]==Token.player1
					&& board[i+1][j+1]==Token.player1
					&& board[i+2][j+2]==Token.player1
					&& board[i+3][j+2]!=Token.empty
					&& board[i+3][j+3]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				j++;
			}
			i++;
		}	
		return getcol ;
	}

	private int checkDiagonalsLeft(Token[][] board) {
		int getcol = -1 ;
		boolean found = false ;
		int i = 0 ;
		while(i<4 && !found){
			int j = 5 ;
			while (j>3 ){
				if(board[i][j]==Token.player2
					&& board[i+1][j-1]==Token.player2
					&& board[i+2][j-2]==Token.player2
					&& board[i+3][j-4]!=Token.empty
					&& board[i+3][j-3]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				else if
						(board[i][j]==Token.player1
					&& board[i+1][j-1]==Token.player1
					&& board[i+2][j-2]==Token.player1
					&& board[i+3][j-4]!=Token.empty
					&& board[i+3][j-3]==Token.empty){
						found = true;
						getcol = i+3;
						break;
					}
				j--;
			}
			i++;
		}	
		return getcol ;

	}
	

	/**
	 * @return true if the column col is already full and false otherwise.
	 */
	private boolean isColFull(int col, Token[][] board) {
		int topRow = board[col].length - 1;
		return (board[col][topRow] != Token.empty);
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public Token getToken() {
		return this.token;
	}

	public String getPlayersName() {
		return "Random Player";
	}
}
