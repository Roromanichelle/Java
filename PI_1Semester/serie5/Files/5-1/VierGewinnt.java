/* ************************************************************************* *\
*                Programmierung 1 HS 2019 - Serie 5-1                         * 
\* ************************************************************************* */

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */


import java.util.Arrays;
import java.util.Scanner;
// import sun.font.TrueTypeFont;


public class VierGewinnt
{

	public static final int COLS = 7;
	public static final int ROWS = 6;

	public Token[][] board = new Token[ COLS ][ ROWS ]; // 7 columns with 6 fields each
	private IPlayer[] players = new IPlayer[ 2 ]; // two players

	/** initialize board and players and start the game */
	public void play()
	{
		// initialize the board
		for ( Token[] column : this.board ) {
			Arrays.fill( column, Token.empty );
		}

		/* initialize players */
		players[ 0 ] = new HumanPlayer();
		System.out.print( "Play against a human opponent? (y / n) " );
		String opponent = new Scanner( System.in ).nextLine().toLowerCase();
		while ( ( 1 != opponent.length() ) || ( -1 == ( "yn".indexOf ( opponent ) ) ) ) {
			System.out.print( "Can't understand your answer. Play against a human opponent? (y / n) " );
			opponent = new Scanner( System.in ).nextLine().toLowerCase();
		}
		if ( opponent.equals( "y" ) ) {
			players[ 1 ] = new HumanPlayer();
		} else {
			players[ 1 ] = new ComputerPlayer();
		}
		players[ 0 ].setToken( Token.player1 );
		players[ 1 ].setToken( Token.player2 );

		/* play... */
		boolean solved = false;
		int currentPlayer = ( new java.util.Random() ).nextInt( 2 );  //choose randomly who begins
		System.out.println( "current player: " + currentPlayer );
		int insertCol, insertRow; // starting from 0
		while ( !solved && !this.isBoardFull() ) {
			// get player's next "move"
			// note that we pass only a copy of the board as an argument,
			// otherwise the player would be able to manipulate the board and cheat!
			insertCol = players[ currentPlayer ].getNextColumn( getCopyOfBoard() );
			// insert the token and get the row where it landed
			insertRow = this.insertToken( insertCol, players[ currentPlayer ].getToken() );
			// check if the game is over
			solved = this.checkVierGewinnt( insertCol, insertRow );
			//switch to other player
			if ( !solved )
				currentPlayer = ( currentPlayer + 1 ) % 2;
		}
		System.out.println( displayBoard( this.board ) );
		if ( solved )
			System.out.println( "Player " + players[ currentPlayer ].getToken() + " wins!" );
		else
			System.out.println( "Draw! Game over." );
	}


	/**
	 * Inserts the token at the specified column (if possible)
	 * @param column the column to insert the token
	 * @param token the players token
	 * @return the row where the token landed 
	 */
	private int insertToken( int column, Token tok )
	{
		//TODO: Your code goes here
		Token a = Token.empty ;
		int i=0;
		if (isBoardFull() == true){
			System.exit(1) ;
		}
		else {
			while (board[column][i] != a) {
				i++;
			}
			board[column][i] = tok ;
		} 
		return i; //TODO: Replace this line
	}


	/**
	 * Checks if every position is occupied 
	 * @returns true, iff the board is full.
	 */
	private boolean isBoardFull()
	{	
		//TODO: Your code goes here
		boolean b = true ;
		Token a = Token.empty ;
		for (int i=0 ; (i<COLS) ; i++){ // we can also do : for (int value : board)
			for (int j=0 ; (j<ROWS) ; j++) {
				if (board[i][j]==a){ // empty case ?
					b = false;
				}
			}
		}
		return b ;
	}


	/**
	 * Checks for at least four equal tokens in a row in
	 * either direction, starting from the given position. 
	 */
	private boolean checkVierGewinnt( int col, int row )
	{
		//TODO: Your code goes here
		if (checkColumns(col, row) == true || checkRows(col, row) == true || checkDiagonalsLeft(col, row) == true || checkDiagonalsRight(col, row) == true) {
			return true ;
		}
		else {
			return false;
		}
	}

	private boolean checkRows(int col, int row) {
		boolean b = false ;
		for (int i=0 ; (i<4); i ++){
			for (int j=0 ; (j<ROWS) ; j++){ 		
				int h = 1;
				while ((board[i][j]==board[col][row]) && (board[i][j]==board[i+h][j]) && (h<4)){
					h++;
				}
				if(h==4){
					b = true ;
				}
			} 
		}
		return b;
	}

	private boolean checkColumns(int col, int row) {
		boolean b = false ;
		for (int i=0 ; (i<COLS) ; i ++){
			for (int j=0 ; (j<3) ; j++){
				int h = 1;
				while ((board[i][j]==board[col][row]) && (board[i][j]==board[i][j+h]) && (h<4)){
					h++;
				}
				if(h==4){
					b = true ;
				}
			}
		}
		return b ;
	}

	private boolean checkDiagonalsRight(int col, int row) {
		boolean b = false ;
		for (int i=0 ; (i<4) ; i ++){
			for (int j=0 ; (j<3) ; j++){
				int h = 1;
				while ((board[i][j]==board[col][row]) && (board[i][j]==board[i+h][j+h]) && (h<4)){
					h++;
				}
				if(h==4){
					b = true ;
				}
			}
		}
		return b ; 
	}

	private boolean checkDiagonalsLeft(int col, int row) {
		boolean b = false ;
		for (int i=0 ; (i<4) ; i++){
			for (int j=5; (j>3) ; j--){
				int h = 1;
				while ((board[i][j]==board[col][row]) && (board[i][j]==board[i+h][j-h]) && (h<4)){
					h++;
				}
				if(h==4){
					b = true ;
				}
			}
		}
		return b ; 
	}

	

	/** Returns a (deep) copy of the board array */
	private Token[][] getCopyOfBoard()
	{
		Token[][] copiedBoard = new Token[ COLS ][ ROWS ];
		for ( int i = 0; i < copiedBoard.length; i++ ) {
			for ( int j = 0; j < copiedBoard[ i ].length; j++ ) {
				copiedBoard[ i ][ j ] = this.board[ i ][ j ];
			}
		}
		return copiedBoard;
	}


	/** returns a graphical representation of the board */
	public static String displayBoard( Token[][] myBoard )
	{
		String rowDelimiter = "+";
		String rowNumbering = " ";
		for ( int col = 0; col < myBoard.length; col++ ) {
			rowDelimiter += "---+";
			rowNumbering += " " + ( col + 1 ) + "  ";
		}
		rowDelimiter += "\n";

		String rowStr;
		String presentation = rowDelimiter;
		for ( int row = myBoard[ 0 ].length - 1; row >= 0; row-- ) {
			rowStr = "| ";
			for ( int col = 0; col < myBoard.length; col++ ) {
				rowStr += myBoard[ col ][ row ].toString() + " | ";
			}
			presentation += rowStr + "\n" + rowDelimiter;
		}
		presentation += rowNumbering;
		return presentation;
	}



	/** main method, starts the program */
	public static void main( String args[] )
	{
		VierGewinnt game = new VierGewinnt();
		game.play();
	}
}
