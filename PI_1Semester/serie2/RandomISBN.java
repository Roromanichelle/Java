/* ************************************************************************* *\
*                Programmierung 1 HS 2018 - Serie 2 Aufgabe 1                 * 
\* ************************************************************************* */

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */


import java.text.DecimalFormat;													// Import the fonction 
import java.util.Random;

public class RandomISBN
{

	public static void main( String args[] )
	{
		// TODO: Insert your code here!

		System.out.println("1st ISBN : " + makeISBN());
		System.out.println("2nd ISBN : " + makeISBN());
		System.out.println("3rd ISBN : " + makeISBN());
		
	}

	/** generates and returns a random ISBN in the format XX-XXX-XX-C */
	public static String makeISBN()
	{
		// Do NOT change the declaration of the following variables!
		String laendercode;
		String bandnr;
		String verlagsnr;
		String checksum;

		// TODO: Insert your code here!
		Random generator = new Random();
		int L1L2;
		int	B1B2B3;
		int V1V2;
		int C;

		L1L2 = generator.nextInt(99) + 1;
		B1B2B3 = generator.nextInt(899) + 100;
		V1V2 = generator.nextInt(99) + 1;
		
		C = (hashOp(L1L2/10) + L1L2%10 + hashOp(B1B2B3/100) + (B1B2B3%100)/10 + hashOp(B1B2B3%10) + V1V2/10 + hashOp(V1V2%10))%10 ;
		
		laendercode = String.format("%02d",L1L2);
		bandnr = Integer.toString(B1B2B3);
		verlagsnr =	String.format("%02d",V1V2);
		checksum = Integer.toString(C);
		
		return laendercode + "-" + bandnr + "-" + verlagsnr + "-" + checksum ;
	
	}


	/** multiplies i with 2 and subtracts 9 if result is >= 10 */
	public static int hashOp(int i)
	{
		// Do NOT change this method!
		int doubled = 2 * i;
		if (doubled >= 10) {
			doubled = doubled - 9;
		}
		return doubled;
	}
}
