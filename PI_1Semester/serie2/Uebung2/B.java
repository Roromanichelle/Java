
/* ************************************************************************* *\
*                Programmierung 1 HS 2018 - Serie 2 Aufgabe 2                 * 
\* ************************************************************************* */

/**
 * @author Romane Carette 19-106-020 / Lina Kadamala Samuel 18-124-115
 * 22.09.2019
 */

 /**
 * 
 * Das Programm produziert der folgende Output :
 * 4/3
 * 5/5
 * ROCK / rock 
 * 2/1
 * 
 *  */  

public class A {
    private int a = 3;
    public void increment (){ a ++; }               // do a+1
    public String toString (){ return ""+ a ;}      // returns a string with the value a 
}


public class B {
    public static void main ( String args []){
        A a1 = new A ();                            // Create a new variable with the class A (a1= 3)
        A a2 = new A ();                            // Create a new variable with the class A (a2= 3)
        a1 . increment ();                          // Do a1 = a1 + 1 (=3+1=4)(class A)
        System . out . println ( a1 +"/"+ a2 );     
        a2 = a1 ;                                   // a2 become a1 (4)
        a2 . increment ();                          // Do a2 = a1 = a1 + 1 (=4+1=5)
        System . out . println ( a1 +"/"+ a2 );     

        String s1 = " ROCK ";                       // Create a string s1 : ROCK
        String s2 = s1 ;                            // Create a string s2 : ROCK (which is the same as S1)
        s2 = s2 . toLowerCase ();                   // Returns the String s2 converted to lowercase
        System . out . println ( s1 +"/"+ s2 );     

        int j =1;                                   // create an integer j with the value 1
        int i = j ;                                 // create an integer i with the value j (=1)
        j ++;                                       // Do j = j + 1 (1+1 = 2)
        System . out . println ( j +"/"+ i );       
    }
}

