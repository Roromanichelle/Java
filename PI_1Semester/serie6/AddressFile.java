import java.util.*;
import java.io.*;

public class AddressFile {

    protected String filename;

    public AddressFile(String filename) {
        this.filename = filename;

    }

    public String toLine(Address addr) {
        return addr.toString();
    }

    public Address parseLine(String str) throws AddressFileException {
        try {
            Scanner sc = new Scanner(str);
            sc.useDelimiter(",");
            String id = sc.next();
            System.out.println(id);
            String id2 = id.trim();
            System.out.println(id2);
            int id3 = Integer.parseInt(id2);

            String name = sc.next().trim();
            String street = sc.next().trim();
            int zipCode = Integer.parseInt(sc.next().trim());
            String city = sc.next().trim();
            sc.close();
            return new Address(id3, name, street, zipCode, city);
        } catch (Exception exception) {
            throw new AddressFileException("Invalid Input");
        }

    }

    public void save(ArrayList<Address> addresses) {

        try {
            BufferedWriter filenameWrite = new BufferedWriter(new FileWriter(new File("filename.txt")));
            for (Address i : addresses) {
                filenameWrite.write(toLine(i));
                filenameWrite.newLine();
                filenameWrite.close();
            }

            /* Other possibility */

            // int i = 0;
            // while (i < addresses.size()) {
            // filenameWrite.write(toLine(addresses.get(i)));
            // filenameWrite.newLine();
            // i++;
            // }

            filenameWrite.close();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public ArrayList<Address> load() throws AddressFileException {
        String currentline = "";
        ArrayList<Address> liste = new ArrayList<Address>();
        try {

            /* Other possibility */

            // BufferedReader filenameRead = new BufferedReader(new FileReader(filename));
            // while ((currentline = filenameRead.readLine()) != null ) {
            // liste.add(parseLine(currentline));
            // }
            // filenameRead.close();
            
            File f = new File(this.filename);
            this.checkFile(f);
            Scanner fileScan = new Scanner(f);
            while (fileScan.hasNextLine()) {
                currentline = fileScan.nextLine();
                liste.add(parseLine(currentline));
            }
            fileScan.close();
            return liste;
        } catch (Exception exception) {
            throw new AddressFileException(exception.getMessage());
        }
    }

    public void checkFile(File f){  // We had problems to read the files with the debuger
                //apply File class methods on File object 
                System.out.println("File name :"+f.getName()); 
                System.out.println("Path: "+f.getPath()); 
                System.out.println("Absolute path:" +f.getAbsolutePath()); 
                System.out.println("Parent:"+f.getParent()); 
                System.out.println("Exists :"+f.exists()); 
                if(f.exists()) 
                { 
                    System.out.println("Is writeable:"+f.canWrite()); 
                    System.out.println("Is readable"+f.canRead()); 
                } 
    }
}
