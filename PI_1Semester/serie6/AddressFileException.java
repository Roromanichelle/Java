
public class AddressFileException extends Exception {

    private static final long serialVersionUID = 1L;

    public AddressFileException(String error_message) {
        super(error_message) ;
    }
}
