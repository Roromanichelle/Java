import java.util.*;
import java.io.*;

public class AddressFileLabelled extends AddressFile {

final private static String REG_STRING = "[\\s]*:[\\s]*([^;]*)";

    public AddressFileLabelled(String filename) {
        super(filename);
    }
    
    @Override
    public String toLine(Address addr) {

        int id = addr.getId();
        String name = addr.getName();
        String street = addr.getStreet();
        int zipCode = addr.getZipCode();
        String city = addr.getCity();
        return "id: " + id + "; name: " + name + "; street: " + street + "; zip: " + zipCode + "; city: " + city + ";";
    }

    @Override
    public Address parseLine(String str) throws AddressFileException {
        try {
            
            Scanner sc = new Scanner(str);

            sc.findInLine("id" + REG_STRING);
            int id = Integer.parseInt(sc.match().group(1).trim());
            
            sc.findInLine("name" + REG_STRING);   
            String name = sc.match().group(1).trim();

            sc.findInLine("street" + REG_STRING);
            String street = sc.match().group(1).trim();

            sc.findInLine("zip" + REG_STRING);
            int zipCode = Integer.parseInt(sc.match().group(1).trim());

            sc.findInLine("city" + REG_STRING);
            String city = sc.match().group(1).trim();
            
            sc.close();
            return new Address(id, name, street, zipCode, city);

        } catch (Exception exception) {
            throw new AddressFileException("Invalid Input");
        }
    }

}
