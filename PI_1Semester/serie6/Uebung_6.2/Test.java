public class Test {
    public static void main(String[] args) {
        new A().bla(); // gibt Hello from A zurück
        new A().foo();
        new A().bar();
        new B().bla();
        new B().foo();
        new B().bar();
        new C().bla();
        new C().foo();
        new C().bar();


/* Hello from A 
 A . foo 
 A . foo 
 Hello from B 
 A . foo 
 A . foo 
 Hello from B 
 C . foo 
 C . foo */